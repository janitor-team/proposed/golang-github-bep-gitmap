golang-github-bep-gitmap (1.1.2-1) unstable; urgency=medium

  * New upstream version 1.1.2
  * Bump Standards-Version to 4.5.0 (no change)
  * Add "Rules-Requires-Root: no" to debian/control
  * debian/gbp.conf: Set debian-branch to debian/sid for DEP-14 conformance

 -- Anthony Fok <foka@debian.org>  Fri, 10 Apr 2020 11:47:47 -0600

golang-github-bep-gitmap (1.1.1-1) unstable; urgency=medium

  * New upstream version 1.1.1
  * Use debhelper-compat and switch to v12

 -- Dr. Tobias Quathamer <toddy@debian.org>  Mon, 09 Sep 2019 10:54:18 +0200

golang-github-bep-gitmap (1.1.0-1) unstable; urgency=medium

  * New upstream version 1.1.0
  * Bump Standards-Version to 4.4.0 (no change)

 -- Anthony Fok <foka@debian.org>  Sat, 27 Jul 2019 02:47:10 -0600

golang-github-bep-gitmap (1.0.0-1) unstable; urgency=medium

  * New upstream version 1.0.0
  * Add debian/watch
  * Update Maintainer email address to team+pkg-go@tracker.debian.org
  * Use debhelper (>= 11~)
  * Bump Standards-Version to 4.2.1 (no change)

 -- Anthony Fok <foka@debian.org>  Thu, 20 Dec 2018 15:17:03 -0700

golang-github-bep-gitmap (0.0~git20180402.012701e-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Anthony Fok ]
  * New upstream version 0.0~git20180402.012701e:
     - Try not to follow symbolic link for TopLevelAbsPaths.
  * Apply "cme fix dpkg" to debian/control,
    bumping Standards-Version to 4.1.3, setting Priority to optional,
    adding Testsuite: autopkgtest-pkg-go, etc.

 -- Anthony Fok <foka@debian.org>  Mon, 02 Apr 2018 20:34:48 -0600

golang-github-bep-gitmap (0.0~git20170613.0.de8030e-1) unstable; urgency=medium

  * New upstream version.
  * Bump Standards-Version to 4.0.0:
    Use https form of the copyright-format URL in debian/copyright.
  * Add myself to the list of Uploaders.

 -- Anthony Fok <foka@debian.org>  Sun, 09 Jul 2017 09:16:47 -0600

golang-github-bep-gitmap (0.0~git20161029.0.a1a71ab-1) unstable; urgency=medium

  * Initial release (Closes: #848700)

 -- Dr. Tobias Quathamer <toddy@debian.org>  Mon, 19 Dec 2016 22:05:59 +0100
